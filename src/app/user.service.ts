import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  public loginApi : string = environment.baseUrl1 + "/users/tokenV2";
  public mappingApi : string = environment.baseUrl2 + "/source_fields";

  token : any = localStorage.getItem('token');
  @Output() arrayForName = new EventEmitter<Array<any>>();

  constructor(private http : HttpClient) {
    // console.log(this.http.get<JSON>(this.apiUrl));
   }

  loginMethod(postData : {username : string; password : string}):Observable<any> {
    // const headers = {'content-type':'application/json'}
    // const body = JSON.stringify(postData);
    return this.http.post<any>(this.loginApi,postData); 
  }

  // Method to get JSON data for mapping component 
  getData():Observable<any>{            
    let headers = new HttpHeaders({'Content-type':'application/json', 'Authorization': `Bearer ${this.token}`});
    return this.http.get(this.mappingApi,{headers});
  }

  getRecords(pageLimitData : {page : number; limit : number}) {
    let virtualScrollApi : string = environment.baseUrl3 + "/users?page=" + pageLimitData.page + "&limit=" + pageLimitData.limit; 
    return this.http.get(virtualScrollApi);
  }
}
