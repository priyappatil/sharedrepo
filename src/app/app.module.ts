import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { HomeComponent } from './home/home.component';
import { MatIconModule } from '@angular/material/icon';
import { SitemapComponent } from './home/sitemap/sitemap.component';
import { BulletlistComponent } from './home/bulletlist/bulletlist.component';
import { BorderallComponent } from './home/borderall/borderall.component';
import { VirtualScrolling } from './home/virtualscrolling/virtualscrolling.component';
import { MappingComponent } from './home/mapping/mapping.component';
import { SettingsComponent } from './home/settings/settings.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NavbarComponent } from './home/navbar/navbar.component';
import { ToolbarComponent } from './home/toolbar/toolbar.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogComponent } from './home/mapping/dialog/dialog.component';
import { FormsModule } from '@angular/forms';
import { CustomerComponent } from './home/customer/customer.component';
import { CustomerDialogComponent } from './home/customer/customer-dialog/customer-dialog.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SearchfilterPipe } from './searchfilter.pipe';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxSpinnerModule } from 'ngx-spinner';
import { OneComponent } from './home/one/one.component';
import { TwoComponent } from './home/two/two.component';
import { ThreeComponent } from './home/three/three.component';
import { ItemComponent } from './home/item/item.component';
import { ProfileComponent } from './home/customer/profile/profile.component';
import { UserComponent } from './home/user/user.component';
import { CreateUserComponent } from './home/user/create-user/create-user.component';

@NgModule({
  
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SitemapComponent,
    BulletlistComponent,
    BorderallComponent,
    VirtualScrolling,
    MappingComponent,
    SettingsComponent,
    NavbarComponent,
    ToolbarComponent,
    DialogComponent,
    CustomerComponent,
    CustomerDialogComponent,
    SearchfilterPipe,
    OneComponent,
    TwoComponent,
    ThreeComponent,
    ItemComponent,
    ProfileComponent,
    UserComponent,
    CreateUserComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,  
    MatIconModule,
    DragDropModule,
    MatDialogModule,
    FormsModule,
    Ng2SearchPipeModule,
    ScrollingModule,
    MatProgressSpinnerModule,
    InfiniteScrollModule,
    NgxSpinnerModule,
    ToastrModule.forRoot()
  ],

  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
