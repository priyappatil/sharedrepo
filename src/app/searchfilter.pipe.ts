import { Pipe, PipeTransform } from '@angular/core';
import { Customer } from './customer';

@Pipe({
  name: 'searchfilter'
})

export class SearchfilterPipe implements PipeTransform {

  transform(customers : Customer[], name : string): Customer[] {

    if( !customers || !name) {
      return customers;
    }

    return customers.filter(customer =>
       customer.firstName.toLocaleLowerCase().includes(name.toLocaleLowerCase()) ||
       customer.lastName.toLocaleLowerCase().includes(name.toLocaleLowerCase()));
  }
}
