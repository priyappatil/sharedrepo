import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth/auth.guard';
import { SitemapComponent } from './home/sitemap/sitemap.component';
import { BulletlistComponent } from './home/bulletlist/bulletlist.component';
import { BorderallComponent } from './home/borderall/borderall.component';
import { VirtualScrolling } from './home/virtualscrolling/virtualscrolling.component';
import { MappingComponent } from './home/mapping/mapping.component';
import { SettingsComponent } from './home/settings/settings.component';
import { CustomerComponent } from './home/customer/customer.component';
import { UserComponent } from './home/user/user.component';

const routes: Routes = [
  { path : 'login', component : LoginComponent },   // routes works according to sequence hence it will redirect to login page first.
  { path : 'home', component : HomeComponent,
    canActivate : [AuthGuard],
    children : [
      { path : '', redirectTo : 'Mapping', pathMatch : 'full' },
      { path : 'Sitemap', component : SitemapComponent },
      { path : 'Bulletlist', component : BulletlistComponent },
      { path : 'Borderall', component : BorderallComponent },
      { path : 'VirtualScrolling', component : VirtualScrolling },
      { path : 'Mapping', component : MappingComponent },
      { path : 'Settings', component : SettingsComponent},
       
      { path : 'Customers', component : CustomerComponent }
  ]},
  { path : 'user', component : UserComponent},
  { path : '**', redirectTo : '/login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
