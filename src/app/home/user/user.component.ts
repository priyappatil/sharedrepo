import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CreateUserComponent } from './create-user/create-user.component';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class UserComponent implements OnInit {

  userArray : any = [];
  isAdmin : boolean = false;
  isUser : boolean = false;
  name : string ="";
  newUser : boolean = false;

  constructor(private route : Router, private dialog : MatDialog) { }

  ngOnInit(): void {
  }

  return() {
    // this.route.navigate(['/home/Settings']);
    this.route.navigateByUrl("/home/Settings")
  }

  openUserDialog() {
    this.newUser = true;

    const dialogRef = this.dialog.open(CreateUserComponent, {
      panelClass : 'custom-dialog-container',
      data : {
        booleanValue : this.newUser
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        // this.userArray = result.data;
        this.userArray.push(result.data);
      }
      
    })
  }

  deleteUser(index : any) {
    this.userArray.splice(index, 1);
  }

  editUser(user : any, index : any) {
    // console.log(user);
    // console.log(index);

    this.newUser = false;

    const dialogRef = this.dialog.open(CreateUserComponent, {
      panelClass : 'custom-dialog-container',
      data : {
        booleanValue : this.newUser,
        currentUser : user
      }
   });

   dialogRef.afterClosed().subscribe(result => {
     if(result) {
       this.userArray[index] = result.data;
     }
   })
  }

}
