import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  newUser : boolean = false;
  userForm : FormGroup;

  constructor(
      private dialog : MatDialog,
      public dialogRef : MatDialogRef<CreateUserComponent>,
      @Optional() @Inject(MAT_DIALOG_DATA) public data : any
    ) { 
    this.userForm = new FormGroup({
      firstName : new FormControl('', Validators.required),
      lastName : new FormControl('', Validators.required),
      email : new FormControl('', 
        [
          Validators.required, 
          Validators.email, 
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]),
      role : new FormControl(''),
    })
  }

  ngOnInit(): void {
    this.newUser = this.data.booleanValue;

    if(this.data.currentUser != null) {
      // console.log(this.data.currentUser);
      this.newUser = this.data.booleanValue;
      this.userForm.patchValue(this.data.currentUser);
    }
  }

  updateUser() {
    this.dialogRef.close({data : this.userForm.value});
  }

  onSubmit() {
    // console.log(this.userForm.value);
    this.dialogRef.close({data : this.userForm.value});
  }

  close() {
    this.dialog.closeAll();
  }

}
