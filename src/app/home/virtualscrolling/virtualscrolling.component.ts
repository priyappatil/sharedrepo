import { CdkVirtualScrollViewport} from '@angular/cdk/scrolling';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { filter, map, pairwise, throttleTime, timeout, timer } from 'rxjs';
import { UserService } from 'src/app/user.service';
// import { CollectionViewer, DataSource } from '@angular/cdk/collections';
// import { BehaviorSubject, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-virtualscrolling',
  templateUrl: './virtualscrolling.component.html',
  styleUrls: ['./virtualscrolling.component.css'],
  changeDetection : ChangeDetectionStrategy.Default
})
export class VirtualScrolling implements OnInit {

  records : any = [];
  editRow : boolean = false;
  isEditable : boolean = false;
  deleteRow : boolean = false;
  pageScrolled : boolean = false;
  page : number = 1;
  limit : number = 10;
  pageIncrement : number = 0;
  limitIncremet : number = 0;
  loading : boolean = false;

  @ViewChild(CdkVirtualScrollViewport) viewPort! : CdkVirtualScrollViewport;
  @ViewChild('changename') changeNameInput !: ElementRef;

  constructor(
    private service : UserService,
    private changeDetectorReference : ChangeDetectorRef,
    private ngZone : NgZone 
    ) { }

  ngOnInit(): void {
    this.getDataFromService(this.page, this.limit);
  }

  ngAfterViewInit():void {
    if(this.limit <= 100) {
      this.viewPort.elementScrolled().pipe(
        map(() => this.viewPort.measureScrollOffset("bottom")),
        pairwise(),
        filter(([top, bottom]) => (bottom < top) && (bottom < 200)),
        throttleTime(1000)      
      ).subscribe(() => {
        this.loading = false
        this.ngZone.run(() => {
          // this.page++;
          this.limit = this.limit + 10;
          this.getDataFromService(this.page, this.limit);
          // this.loading = true;
        })
      });
    } else {
      this.loading = false;
    }
    
  }

  getDataFromService(page : number, limit : number) {
    this.loading = true;
    if(this.limit <= 100) {
      this.service.getRecords({page : page, limit : limit}).subscribe(result => {
        if(result) {
          this.records = result;
        } 
  
          // this.loading = true;
         
          timer(1000).subscribe(() => {
            if(result) {
              // this.loading = false;
              this.records = result;
              this.loading = false;
            }
          })
          setTimeout(() => {
            this.loading = false;
          }, 10000) 
      })
    }
    
  }

  editRecord(record : any, index : number) {
    // this.editRow = !this.editRow;
    this.isEditable = true;

    setTimeout(() => {
      this.changeNameInput.nativeElement.focus(); 
    });
    // const editRow : boolean = true;
    // this.records[index] = record;
    this.records[index].name = record;
  }

  focusOutMethod(index : any) {
    this.isEditable = false;
  }

  focusMethod() {
    this.changeNameInput.nativeElement.focus();
  }

  deleteRecord(index : number) {
    this.records.splice(index, 1);
    this.changeDetectorReference.detectChanges();
  }

  // ngAfterViewInit(): void {
  //   this.scrollDispatcher.scrolled()
  //     .subscribe(event => {
  //       console.log('scrolled');
  //     });
  // }

  scrolledIndexChangeMethod(event : any) {
    // console.log(event);
    console.log(this.records);
  }
}
