import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  isToggled : boolean = false;
  toolbarName : string = "";
  currentRoute : any;
  findResult : any;

  constructor( private service : UserService, private router : Router ) { 

    this.currentRoute =  this.router.url.split("/").pop();
    console.log(this.currentRoute);

    this.service.arrayForName.subscribe(result => {

      this.findResult =  result.find(result => result.name == this.currentRoute);    // find method will return an object which matches the condition
      // console.log(this.findResult.name);
      
      this.toolbarName = this.findResult.name;
    })
   }

  ngOnInit(): void {}

  getName(name : string) {
    this.toolbarName = name;
  }

  getToggleValue(value : boolean) {
    this.isToggled = value;
  }
 }
