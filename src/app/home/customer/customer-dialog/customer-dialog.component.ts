import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-customer-dialog',
  templateUrl: './customer-dialog.component.html',
  styleUrls: ['./customer-dialog.component.css']
})
export class CustomerDialogComponent implements OnInit {

  customerForm! : FormGroup;
  newCustomer : boolean = false;
  existingCustomer : any;

  constructor(
    public dialogRef : MatDialogRef<CustomerDialogComponent>,
    private dialog : MatDialog,
    //  @Optional is used to prevent error if no data is passed 
    @Optional() @Inject(MAT_DIALOG_DATA) public data : any
    ) {
        this.customerForm = new FormGroup({
        firstName : new FormControl('', Validators.required),
        lastName : new FormControl('', Validators.required),
        gender : new FormControl('', Validators.required),
        email : new FormControl('', 
          [
            Validators.required, 
            Validators.email, 
            Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
          ]),
        address : new FormControl(''),
        city : new FormControl(''),
        state : new FormControl(''),
        // profilePic : new FormControl('')
      })
     }

  ngOnInit(): void {

    this.newCustomer = this.data.booleanValue;

    if(this.data.index != null) {
      this.existingCustomer = this.data.index;
      this.customerForm.patchValue(this.existingCustomer);
    }
  }

  closeDialog() { 
    this.dialogRef.close();
  }

  onSubmit() {
    // console.log(this.customerForm.value);
    this.dialogRef.close({ event : 'submit', data : this.customerForm.value});
  }

  updateCustomer() {
    this.dialogRef.close({ event : 'update', data : this.customerForm.value});
  }

  deleteCustomer() {
    this.dialogRef.close({ event : 'delete'});
  }

  getTitle() {
    if (this.newCustomer) {
      return 'New Customer';
    } else {
      return this.existingCustomer.firstName + " " + this.existingCustomer.lastName;
    }
  }

  uploadProfileImage() {
    console.log("Clicked");   
  }

  onSelectFile(event : any) {
    if(event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      // reader.onload = (event) => {
      //   this.profilePic = event.target?.result;
      //   console.log(this.profilePic);
      // }
    }
  }
}
