import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CustomerDialogComponent } from './customer-dialog/customer-dialog.component';
import { Customer } from 'src/app/customer';
import { ProfileComponent } from './profile/profile.component';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  newCustomer : boolean = false;
  customersArray : any = [];
  searchName : string = "";
  name : string = ""; 
  // Customer Interface is for filter functionality
  customerInterface! : Customer[];
  profilePicUrl : any;
  changeProfileBoolean : boolean = false;

  constructor(private dialog : MatDialog) { }

  ngOnInit(): void {}

  openDialog() {
    this.newCustomer = true;
    if(this.customersArray.length === 0 || this.customersArray.length > 0) {   
      const dialogRef =  this.dialog.open(CustomerDialogComponent,{
        data : {
          booleanValue : this.newCustomer
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if(result) {
          this.customersArray.push(result.data);
          console.log(this.customersArray);
          // console.log(result.data.profilePic);
          // this.profilePicUrl = result.data.profilePic;
          // this interface is for filter functionality 
          this.customerInterface = this.customersArray;
        }
      })
    }
  }

  openCustomDialog(customerData : any, index : any) {
    this.newCustomer = false;

    if(this.customersArray.length > 0) {
      const dialogRef = this.dialog.open(CustomerDialogComponent, {
        data : {
          booleanValue : this.newCustomer,
          index : customerData
        }
     });

    dialogRef.afterClosed().subscribe(result => {

      if(result) {
        if(result.event === "update") {
          this.customersArray[index] = result.data;
        }

        if(result.event === "delete") {
          this.customersArray.splice(index, 1);
        }
      }
      // this.customersArray.pop(index);     
     })
    }
  }

  changeProfile(customerData : any, index : any) {
    const dialogRef =  this.dialog.open(ProfileComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.changeProfileBoolean = true;
      
      this.profilePicUrl = result.data;
      // this.customersArray[index].profilePic = result.data;
      console.log(this.profilePicUrl);
    })
  }
}
