import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  url : any;

  constructor(
    public dialogRef : MatDialogRef<ProfileComponent>
  ) { }

  ngOnInit(): void {
  }

  onSelectFile(event : any) {
    if(event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event) => {
        this.url = event.target?.result;
        console.log(this.url);
      }
    }
  }

  saveChanges() {
    console.log("Profile Changed"); 
    console.log(this.url);
    this.dialogRef.close({data : this.url});
  }
}
