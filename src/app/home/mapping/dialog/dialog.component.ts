import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserService } from 'src/app/user.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  // encapsulation: ViewEncapsulation.None
})
export class DialogComponent implements OnInit {

  // sliderValue : any;
  // It is because of the typescript version which includes a strict class checking where all the properties should be initialized in the constructor (! -> definite assignment assertion operator => will tell the compiler that it wil be initialized at run time)
  is_Issue_field! : boolean;
  is_user_field! : boolean;
  isRequired : boolean = false;
  isDropdown : boolean = false;
  isAIML : boolean = false;
  isAIMLActive : boolean = false;
  isAutomated : boolean = false;
  isIssueField : boolean = false;
  isAnalyticsField : boolean = false;
  progressBarValue : number = 0;
  // sliderForm : FormGroup;

  constructor (
    private dialog : MatDialog, 
    private service : UserService, 
    @Inject(MAT_DIALOG_DATA) public data : any ,
    // @Inject(FormBuilder) public form : FormBuilder
  ) { 
    //  this.sliderValue = data.ml_prediction_threshold;
    //  this.sliderForm = this.form.group({
    //    'slider' : [0, Validators.min(0)]
    //  });
  }

  ngOnInit(): void {

    // console.log(this.data);

    this.progressBarValue = this.data.ml_prediction_threshold;

    if(this.data.is_Issue_field_readonly) {
      this.is_Issue_field = this.data.is_Issue_field;
    } else {
      this.is_user_field = this.data.is_user_field;
    }

    if(this.is_Issue_field || this.is_user_field) {
      this.isRequired = !this.isRequired;
    }

    if(this.data.is_AIML_field) {
      if(this.data.is_automated) {
        this.isAIML = !this.isAIML;
        this.isAutomated = !this.isAutomated;
        this.progressBarValue = this.data.ml_prediction_threshold;
      } 
    }

    if(this.data.is_AIML_field && !this.data.is_automated) {
      this.isAIML = !this.isAIML;
      this.isAIMLActive = !this.isAIMLActive;
    }
  }

  AIMLClicked() {

    this.isAIMLActive = !this.isAIMLActive;
    this.isAutomated = false;

    if(this.isAIMLActive || !this.isAutomated) {
      this.isAIML = !this.isAIML;
    } else {
      this.isAIML = !this.isAIML;
      this.isAutomated = !this.isAutomated;
    }
  }

  applyAutomationClicked() {
    this.isAutomated = !this.isAutomated;
  }

  close() {
    this.dialog.closeAll();
  }
}
