import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { CdkDragDrop, moveItemInArray, DragDropModule } from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-mapping',
  templateUrl: './mapping.component.html',
  styleUrls: ['./mapping.component.css'],
  encapsulation : ViewEncapsulation.None
})

export class MappingComponent implements OnInit {

  dataArray : any = [];

  constructor( private service : UserService, private dialog : MatDialog ) { }

  ngOnInit(): void {
    this.service.getData().subscribe(result => {
      this.dataArray = result.fields;
      // console.log(this.dataArray);
    })
  }

  drop(event : CdkDragDrop<any[]>) {
    moveItemInArray(this.dataArray, event.previousIndex, event.currentIndex);
  }

  popWindow(index : any) {
    this.dialog.open(DialogComponent,{
      panelClass : 'custom-dialog-container',
      data : index
    });
  }

  readWriteMethod( isIssueFieldReadOnly : boolean, isIssueField : boolean, isUserField : boolean) {

    if(isIssueFieldReadOnly) {

      if(isIssueField) {
        return "Read / Required"
      } else {
        return "Read / Not Required"
      }
    } else {

      if(isUserField) {
        return "Write / Required"
      } else {
        return "Write / Not Required"
      }
    }
  }

  vectorMethod(isAutomated : boolean, mlPredictionThreshold : number) {
    if(isAutomated) {
      return "Vector / Autoamted / " + mlPredictionThreshold
    } else {
      return "Vector"
    }
  }
}
