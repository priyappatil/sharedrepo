import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bulletlist',
  templateUrl: './bulletlist.component.html',
  styleUrls: ['./bulletlist.component.css']
})
export class BulletlistComponent implements OnInit {

  constructor(private route : Router) { }

  ngOnInit(): void { }

  openUserComponent() {
    console.log("Inside User Component");
    this.route.navigate(['/user']);
  }

}
