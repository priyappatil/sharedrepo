import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.css']
})
export class TwoComponent implements OnInit {

  numbers : any = [];
  ds = new MyDataSource();
  url : any = "https://helostatus.com/wp-content/uploads/2021/09/2021-profile-WhatsApp-hd.jpg"

  constructor() { }

  ngOnInit(): void { }

  loadNext() {
    console.log("Page Scrolled");
  }

  onSelectFile(event : any) {
    if(event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event) => {
        this.url = event.target?.result;
      }
    }
  }
}

export class MyDataSource extends DataSource<string | undefined> {
  private _length = 100;
  private _pageSize = 100;
  private _cachedData = Array.from<string>({length : this._length});
  private _fetchedPages = new Set<number>();
  private readonly _dataStream = new BehaviorSubject<(string | undefined)[]>(this._cachedData);
  private readonly _subscription = new Subscription();

  connect(collectionViewer: CollectionViewer): Observable< (string | undefined)[]> {
      this._subscription.add(
        collectionViewer.viewChange.subscribe(range => {
          const startPage = this._getPageForIndex(range.start);
          const endPage = this._getPageForIndex(range.end - 1);
          for(let i = startPage; i <= endPage; i++){
            this._fetchPage(i);
          }
        }),
      );
    return this._dataStream;
  }

  disconnect(): void {
      this._subscription.unsubscribe();
  }

  private _getPageForIndex(index : number):number{
    return Math.floor(index / this._pageSize);
  }

  private _fetchPage(page : number) {
    if(this._fetchedPages.has(page)) {
      return;
    }
    this._fetchedPages.add(page);

    setTimeout(() => {
      this._cachedData.splice(
        page * this._pageSize,
        this._pageSize,
        ...Array.from({length: this._pageSize}).map((_, i)=> `Item #${page * this._pageSize + i}`),
      );
      this._dataStream.next(this._cachedData);
    }, Math.random() * 1000 + 200);
  }
}  
