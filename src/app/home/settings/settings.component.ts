import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  numbers : any = [];
  ds = new MyDataSource();

  constructor(private router : Router) {
    for(let i = 0; i < 100; i++) {
      this.numbers.push(i);
    } 
   }

  ngOnInit(): void { }

  openUserPage() {
    this.router.navigate(['/user'])
    // this.router.navigateByUrl('/user');
  }

  openTeamsPage() {

  }

}

export class MyDataSource extends DataSource<string | undefined> {
  private _length = 100;
  private _pageSize = 100;
  private _cachedData = Array.from<string>({length : this._length});
  private _fetchedPages = new Set<number>();
  private readonly _dataStream = new BehaviorSubject<(string | undefined)[]>(this._cachedData);
  private readonly _subscription = new Subscription();

  connect(collectionViewer: CollectionViewer): Observable< (string | undefined)[]> {
      this._subscription.add(
        collectionViewer.viewChange.subscribe(range => {
          const startPage = this._getPageForIndex(range.start);
          const endPage = this._getPageForIndex(range.end - 1);
          for(let i = startPage; i <= endPage; i++){
            this._fetchPage(i);
          }
        }),
      );
    return this._dataStream;
  }

  disconnect(): void {
      this._subscription.unsubscribe();
  }

  private _getPageForIndex(index : number):number{
    return Math.floor(index / this._pageSize);
  }

  private _fetchPage(page : number) {
    if(this._fetchedPages.has(page)) {
      return;
    }
    this._fetchedPages.add(page);

    setTimeout(() => {
      this._cachedData.splice(
        page * this._pageSize,
        this._pageSize,
        ...Array.from({length: this._pageSize}).map((_, i)=> `Item #${page * this._pageSize + i}`),
      );
      this._dataStream.next(this._cachedData);
    }, Math.random() * 1000 + 200);
  }
}  