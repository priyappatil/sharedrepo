import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {

  isExpanded : boolean = false;
  showDropdown! : boolean;
  @Output() onIconClick = new EventEmitter<string>();
  @Output() toggleEvent = new EventEmitter<boolean>();

  itemArray : any = [
    // {
    //   icon : "dashboard",
    //   name : "Sitemap",
    //   link : "Sitemap"
    // }, 
    {
      icon : "format_list_bulleted",
      name : "Lead5",
      link : "Bulletlist"
    },
    {
      icon : "border_all",
      name : "Borderall",
      link : "Borderall"
    },
    {
      icon : "add_chart",
      name : "VirtualScrolling",
      link : "VirtualScrolling"
    },
    {
      icon : "tune",
      name : "Mapping",
      link : "Mapping" 
    },
    {
      icon : "settings",
      name : "Settings",
      link : "Settings"
    },
    {
      icon : "people",
      name : "Customers",
      link : "Customers"
    }
  ];

  constructor( private service : UserService ) { }

  ngOnInit(): void {
    this.service.arrayForName.emit(this.itemArray);
  }

  toggleSidenav() {
    this.isExpanded = !this.isExpanded;
    this.toggleEvent.emit(this.isExpanded);
  }

  showHeading(name : string) {
    this.onIconClick.emit(name);
    if(name == 'Settings') {
      this.showDropdown = true;
    } else {
      this.showDropdown = false;
    }
  }

  showPopover() {
 
  }

  hidePopover() {

  }
}
