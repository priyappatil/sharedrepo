import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})

export class LoginComponent implements OnInit {

  showPassword: boolean = false;
  passwordType: string = 'password';
  loginForm!: FormGroup;
  user : any = {};

  constructor(private user_Service: UserService, private toastr: ToastrService, private route : Router) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', [Validators.minLength(8), Validators.required]),
      rememberMe : new FormControl(false)
    })

    if(localStorage.getItem('User') && (!localStorage.getItem('token'))) {
      let stringData : any = localStorage.getItem("User"); 
      let user  = JSON.parse(stringData);

      this.loginForm.setValue({
        'username' : user.username,
        'password' : user.password,
        'rememberMe' : true
      })
    }

    if(!this.loginForm.value.rememberMe) {
      localStorage.removeItem('User');
    }

    if(localStorage.getItem('token')) {
      this.route.navigate(['/home']);
    }
  }

  togglePassword() {
    if (this.passwordType == 'password') {
      this.passwordType = 'text';
      this.showPassword = true;
    } else {
      this.passwordType = 'password';
      this.showPassword = false;
    }
  }

  onSubmit() {
    this.user_Service.loginMethod({username: this.loginForm.value.username, password : this.loginForm.value.password }).subscribe(result => {
      if (result.response == "success") {
        // this.toastr.success(result.message, result.response);
        localStorage.setItem('token', result.token);
      
        if(this.loginForm.value.rememberMe) {
          // this.user = Object.assign(this.user, this.loginForm.value),
          this.user = this.loginForm.value;
          localStorage.setItem("User",JSON.stringify(this.user))
          // console.log(JSON.stringify(localStorage.getItem('User'))) ;

        } else {
          localStorage.removeItem('User');
        }

        this.route.navigate(['/home/Mapping']);
        // localStorage.setItem('MaestroUser', JSON.stringify(this.loginForm.value));
      }
    }, Error => {
      this.toastr.error(Error.error.message, Error.error.title);
      console.log(Error.error);
    });
  }
}
