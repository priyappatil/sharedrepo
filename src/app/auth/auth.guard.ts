import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service'; 
// import { LoginComponent } from '../login/login/login.component'; 

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router : Router) {}

  canActivate(route :ActivatedRouteSnapshot, state : RouterStateSnapshot) {
    if(localStorage.getItem('token')) {
      return true;
      // if(localStorage.getItem('User')) {
      //   this.login.loginForm.setValue({
      //     username : localStorage.getItem('username'),
      //     password : localStorage.getItem('password')
      //   })
      // }
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
  
}
